package Mathematics.Complex;

import java.util.ArrayList;

public class MainRun {

    public static void main(String[] args) {

        Complex z1= new Complex(-1,3);
        Complex z2= new Complex(5,-2);

        Complex rez;

        Complex.getListComplex().add(z1);
        Complex.getListComplex().add(z2);

        System.out.println("Z1:");
        System.out.println(z1.toString());

        System.out.println("Z2:");
        System.out.println(z2.toString());
        System.out.println("*****************************************************************");
        rez=z1.add(z2);
        System.out.println("Addition... Z1+Z2= "+rez.toString());
        Complex.getListComplex().add(rez);
        rez=z1.sub(z2);
        System.out.println("Subtraction... Z1-Z2= "+rez.toString());
        Complex.getListComplex().add(rez);
        rez=z1.mul(z2);
        System.out.println("Multiplication... Z1*Z2= "+rez.toString());
        Complex.getListComplex().add(rez);

        try {
            rez=z1.div(z2);
            System.out.println("Division... Z1/Z2= "+rez.toString());
            Complex.getListComplex().add(rez);
        } catch (Exception e) {
            System.out.println("Divide error: "+e.getMessage());
        }
        System.out.println("*****************************************************************");
        System.out.println("Sortiranje kompleksnih brojeva po modulu :");
        //Sortiranje kompleksnih brojeva po modulu :
        Complex.sortByMod();
        Complex.printList();
        System.out.println("*****************************************************************");
        System.out.println("Sortiranje kompleksnih brojeva po uglu:");
        //Sortiranje kompleksnih brojeva po uglu:
        Complex.sortByAngle();
        Complex.printList();

    }

}
