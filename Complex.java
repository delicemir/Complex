package Mathematics.Complex;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Complex {

    private double Re;
    private double Im;

    private static ArrayList<Complex> listComplex = new ArrayList<>();

    public Complex(double re, double im) {
        setRe(re);
        setIm(im);
    }

    public double getRe() {
        return Re;
    }

    public void setRe(double re) {
        Re = re;
    }

    public double getIm() {
        return Im;
    }

    public void setIm(double im) {
        Im = im;
    }

    public static ArrayList<Complex> getListComplex() {
        return listComplex;
    }

    public double abs(double x) {
        return Math.abs(x);
    }

    public double getAngle() {
        if(getRe()==0&& getIm()==0) {
            return 0;
        }
        else if(getRe()>0 && getIm()==0) {
            return 0;
        }
        else if(getRe()<0 && getIm()==0){
            return 180;
        }
        if(getIm()>0) {
            if (getRe() > 0) {
                return Math.toDegrees(Math.atan(abs(getIm() / getRe())));
            } else if(getRe()==0) {
                return 90;
            }
            else {
                return 180-Math.toDegrees(Math.atan(abs(getIm() / getRe())));
            }
        }
        else {
            if(getRe() < 0) {
                return 180+Math.toDegrees(Math.atan(abs(getIm() / getRe())));
            }
            else if(getRe()==0) {
                return 270;
            }
            else {
                return 360-Math.toDegrees(Math.atan(abs(getIm() / getRe())));
            }
        }
    }

    public Complex add(Complex z) {
        Complex num= new Complex(0,0);
        num.setRe(getRe()+z.getRe());
        num.setIm(getIm()+z.getIm());
        return num;
    }

    public Complex sub(Complex z) {
        Complex num= new Complex(0,0);
        num.setRe(getRe()-z.getRe());
        num.setIm(getIm()-z.getIm());
        return num;
    }

    public double getMod() {
        return Math.sqrt(getRe()*getRe()+getIm()*getIm());
    }

    public double getMod(Complex z) {
        return Math.sqrt((z.getRe()*z.getRe())+(z.getIm()*z.getIm()));
    }

    public Complex mul(Complex z) {
        Complex num= new Complex(0,0);
        num.setRe(getRe()*z.getRe()-getIm()*z.getIm());
        num.setIm(getIm()*z.getRe()+getRe()*z.getIm());
        return num;
    }

    public Complex div(Complex z) throws IllegalArgumentException {
        Complex num= new Complex(0,0);
        if(getMod(z)!=0) {
            num.setRe((getRe()*z.getRe()+getIm()*z.getIm())/(getMod(z)*getMod(z)));
            num.setIm((getIm()*z.getRe()-getRe()*z.getIm())/(getMod(z)*getMod(z)));
        }
        else {
            throw new IllegalArgumentException("Modul ne smije biti nula!");
        }

        return num;
    }

    @Override
    public String toString() {
        //return "Complex{" +"Re=" + Re +", Im=" + Im +"}"+"{|Z|= "+mod()+", angle= "+angle()+"°}";
        return String.format("Complex: {Re: %.3f , Im: %.3f} , {|Z|= %.3f , Angle= %.3f°}", getRe(), getIm(),getMod(),getAngle());
    }

    public static void printList(){
        for (Complex z: Complex.getListComplex()) {
            System.out.println(z.toString());
        }
    }

    public static void sortByMod() {
        Collections.sort(getListComplex(), new Comparator<Complex>() {
            @Override
            public int compare(Complex z1, Complex z2) {
                if(z1.getMod()<z2.getMod()){
                    return -1;
                }
                else if(z1.getMod()>z2.getMod()){
                    return 1;
                }
                return 0;
            }
        });
    }

    public static void sortByAngle() {
        Collections.sort(getListComplex(), new Comparator<Complex>() {
            @Override
            public int compare(Complex z1, Complex z2) {
                if(z1.getAngle()<z2.getAngle()){
                    return -1;
                }
                else if(z1.getAngle()>z2.getAngle()){
                    return 1;
                }
                return 0;
            }
        });
    }

}
